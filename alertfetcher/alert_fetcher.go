package alertfetcher

import (
	"fmt"

	_ "gitlab.com/albandewilde/init/locationfetcher"
	_ "gitlab.com/albandewilde/init/userfetcher"
)

func init() {
	fmt.Println("initialize the alert fetcher package")
}
