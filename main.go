package main

import (
	"fmt"

	_ "gitlab.com/albandewilde/init/alertfetcher"
	_ "gitlab.com/albandewilde/init/alertupdater"
	_ "gitlab.com/albandewilde/init/classifiedfetcher"
	_ "gitlab.com/albandewilde/init/programfetcher"
)

func main() {
	fmt.Println("Starting the main program")
}
